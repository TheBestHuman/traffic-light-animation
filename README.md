# Traffic Light Animation - JavaScript Application


![A screenshot of the application showing a traffic light set to red](/screenshot.png "Traffic Light Screenshot")


This is a simple JavaScript application that displays an animated traffic light. The traffic light cycles through red, yellow, and green lights, simulating the behaviour of a real traffic light. Each light has a different duration that it stays on for, specified in seconds.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You will need a modern web browser that supports HTML5, CSS, and JavaScript.

### Installing

Clone the repository to your local machine:

```
git clone <repo-url>
```

## File Structure

The application consists of:

1. An HTML file that creates the structure of the traffic light.
2. A CSS file (style.css) that styles the traffic light, including the housing and the lights.
3. A JavaScript block within the HTML file that controls the logic of the traffic light.

## How it works

The traffic light animation is managed through JavaScript and jQuery. There are three main functions in the JavaScript block:

1. `setRed()`: Turns the traffic light to red and sets a timer to change it to green after `redlight_time` seconds.
2. `setYellow()`: Turns the traffic light to yellow and sets a timer to change it to red after `yellowlight_time` seconds.
3. `setGreen()`: Turns the traffic light to green and sets a timer to change it to yellow after `greenlight_time` seconds.

At the start of the application, the `setRed()` function is called, setting the first light to red. From then on, each function calls the next in a loop, creating the traffic light animation.

## Usage

To see the traffic light in action, simply open the HTML file in your web browser.

## Contributing

If you want to contribute to this project and make it better, your help is very welcome. Create a pull request with your suggested changes, and the changes will be reviewed.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgments

This application uses the jQuery library, hosted by Google.